package Adaptador;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Driver;
import java.time.Duration;
import java.util.List;
import java.util.NoSuchElementException;

import org.apache.commons.io.FileUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.openqa.selenium.By;
import org.openqa.selenium.ElementClickInterceptedException;
import org.openqa.selenium.ElementNotInteractableException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import Listener.EventFiringDecorator;
import Listener.WebDriverListener;

public class AdaptadorSelenium {
	
	private WebDriver driver;
	
	private static AdaptadorSelenium adaptador = null;
	
	String evidencePath = "";
	
	public static AdaptadorSelenium getAdaptador(String browserType, String driverPath) {
		
		if(adaptador == null) {
			adaptador = new AdaptadorSelenium(browserType, driverPath);
		}
		
		return adaptador;
		
	}
	
	public AdaptadorSelenium(String browserType, String driverPath) {
		
		try {
			
			if(browserType.equals("Chrome")) {
				System.setProperty("webdriver.chrome.driver", driverPath);
				driver = new ChromeDriver();
			}else if(browserType.equals("Firefox")) {
				System.setProperty("webdriver.gecko.driver", driverPath);
				driver = new FirefoxDriver();
			}
			
			driver.manage().window().maximize();
			driver.manage().deleteAllCookies();
			driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(30));
			
			WebDriverListener listener = new WebDriverListener();
			this.driver                = new EventFiringDecorator(listener).decorate(this.driver);
			
		}catch(WebDriverException e) {
			
			Assert.fail("No se puede instanciar el driver: " + e.getMessage());
			
		}

	}
	
	public void openUrl(String url) {
		
		driver.get(url);
		
	}
	
	public void closeDriver() {
		
		driver.quit();
		
	}
	
	public void inputText(By localizador, String text) {
		
		WebElement elemento = createElement(localizador, "INGRESAR");
		elemento.clear();
		elemento.sendKeys(text);
		
	}
	
	public void inputTextByElement(WebElement element, String text) {

		element.clear();
		element.sendKeys(text);
		
	}
	
	public void toClick(By localizador) {
		
		try {
			
			createElement(localizador, "CLICK").click();
			
		}catch(ElementClickInterceptedException e){
			
			System.out.println("El elemento con la ruta: " + localizador + " no se le puede hacer click.");
			Assert.fail("El elemento con la ruta: " + localizador + " no se le puede hacer click.");
			
		}
		
	}
	
	public void toClickByElement(WebElement element) {
		
		try {
			
			element.click();
			
		} catch (ElementClickInterceptedException e) {
			
			System.out.println("Al elemento no se le puede hacer click.");
			Assert.fail("Al elemento no se le puede hacer click.");
			
		}
		
	}
	
	public String getText(By localizador) {
		
		String text = "";
		
		try {
			
			text = createElement(localizador, "OBTENER").getText();
			
		}catch(ElementNotInteractableException e){
			
			System.out.println("El elemento con la ruta: " + localizador + " no se puede interactuar.");
			Assert.fail("El elemento con la ruta: " + localizador + " no se puede interactuar.");
			
		}
		
		return text;
		
		
	}
	
	public WebElement createElement(By localizador, String action) {
		
		WebDriverWait wait  = new WebDriverWait(driver, Duration.ofSeconds(20));
		WebElement element  = null;
		
		try {
			
			if(action.equals("CLICK")) {
				
				element = wait.until(ExpectedConditions.elementToBeClickable(localizador));
				
			}else if(action.equals("INGRESAR")) {
				
				element = wait.until(ExpectedConditions.presenceOfElementLocated(localizador));
				
			}else if(action.equals("OBTENER")) {
				
				element = wait.until(ExpectedConditions.visibilityOfElementLocated(localizador));
				
			}else if(action.equals("SELECCIONAR")) {
				
				element = wait.until(ExpectedConditions.visibilityOfElementLocated(localizador));
				
			}else {
				
				element = wait.until(ExpectedConditions.visibilityOfElementLocated(localizador));
				
			}
			
		}catch(NoSuchElementException e){
			
			System.out.println("El elemento con la ruta: " + localizador + " no existe.");
			Assert.fail("El elemento con la ruta: " + localizador + " no existe.");
			
		}
		
		return element;
		
	}
	
	public void select(By localizador, String selectionType, String value) {
		
		Select elementToSelect = new Select(createElement(localizador, "SELECCIONAR"));
		
		if(selectionType.equals("value")) {
			
			elementToSelect.selectByValue(value);
			
		} else if(selectionType.equals("text")) {
			
			elementToSelect.selectByVisibleText(value);
			
		}
		
	}
	
	public boolean getExistingElement(By localizador) {
		
		return createElement(localizador, "").isDisplayed();
		
	}
	
	public void takeEvidence(String path) throws IOException {
		
		try {
			
			String imageName = "img" + getCounter() + ".png";
			
			TakesScreenshot scrShot = ((TakesScreenshot) driver);
			File SrcFile = scrShot.getScreenshotAs(OutputType.FILE);
			File DestFile = new File(path + "/" + imageName);
			FileUtils.copyFile(SrcFile, DestFile);
			
		} catch (Exception e) {
			
			System.out.println("No se puede realizar la captura de pantalla.");
			
		}
		
	}
	
	public boolean elementIsDisplayed(By localizador) {
		
		try {
			
			driver.findElement(localizador);
			return true;
			
		}
		catch (org.openqa.selenium.NoSuchElementException e) {
			
			return false;
			
		}
		
	}
	
	public JSONObject readJSON(String jsonName) throws IOException, ParseException {
		
		JSONParser jsonParser = new JSONParser();
		FileReader reader     = new FileReader(jsonName);
		
		//Read JSON file
		Object obj             = jsonParser.parse(reader);
		JSONObject contactData = (JSONObject) obj;
		
		return contactData;
		
	}
	
	public WebDriver getDriver() {
		
		return driver;
		
	}
	
	int counter = 0;
	
	public int getCounter() {
		
		counter++;
		return counter;
		
	}
	
}
