package Administrator;

import java.io.IOException;
import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.io.FileWriter;



public class AdministratorSelenium {
	
	WebDriver driver;
	
	String textEmail    = "";
	String evidencePath = "";
	
//Get Email.
	String pathTempEmail   = "https://temp-mail.org/es/";
	By byInputEmail        = By.id("mail");
	By byInputEmailVisible = By.xpath("//input[@id='mail' and @class='emailbox-input opentip']");
	
//	Login.
	By byEmail           = By.id("input-email");
	By byPassword        = By.id("input-password");
	By byBtnLogin        = By.xpath("//input[@type='submit' and @value='Login']");
	By byMyAccountTittle = By.xpath("//div[@class='row']//h2[1]");

//	LogOut.
	By byBtnLogout       = By.xpath("//a[@class='list-group-item'][normalize-space()='Logout']");
	
	
	
	public AdministratorSelenium( WebDriver driverD, String evidencePath ) {
		
		driver            = driverD;
		this.evidencePath = evidencePath;
		
	}
	
	
	
	public void openUrlTempEmail() {
		
		driver.get(pathTempEmail);
		
	}
	
	public void closeDriver() {
		
		driver.quit();
		
	}
	
	public String getEmail() throws IOException {
			
		WebDriverWait wait  = new WebDriverWait(driver, Duration.ofSeconds(60));
		WebElement element  = wait.until(ExpectedConditions.visibilityOfElementLocated(byInputEmailVisible));
		element.isDisplayed();
		
		element   = wait.until(ExpectedConditions.visibilityOfElementLocated(byInputEmail));
		textEmail = element.getAttribute("value");

		this.writeTxt(textEmail);
		
		return textEmail;
		
	}
	
	public void writeTxt(String textEmail) throws IOException {
		
		File fileEmails       = new File("src/txt/fileEmails.txt");
		FileWriter writeEmail = new FileWriter(fileEmails,true);
		writeEmail.write("\r\n" + textEmail);
		writeEmail.close();
		
	}
	
	public void doLogin(String email, String password) throws IOException {
		
		WebDriverWait wait         = new WebDriverWait(driver, Duration.ofSeconds(60));
		
		WebElement emailElement    = wait.until(ExpectedConditions.presenceOfElementLocated(byEmail));
		WebElement passwordElement = wait.until(ExpectedConditions.presenceOfElementLocated(byPassword));
		WebElement btnElement      = wait.until(ExpectedConditions.elementToBeClickable(byBtnLogin));
		
		emailElement.clear();
		passwordElement.clear();
		
		emailElement.sendKeys(email);
		passwordElement.sendKeys(password);
		btnElement.click();
		
	}
	
	public void doLogOut() throws IOException, InterruptedException {
		
		WebDriverWait wait   = new WebDriverWait(driver, Duration.ofSeconds(20));
		
		WebElement btnLogOut = wait.until(ExpectedConditions.elementToBeClickable(byBtnLogout));

		btnLogOut.click();
		
	}
	
}
