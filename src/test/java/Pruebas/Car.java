package Pruebas;


import java.io.IOException;

import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import Administrator.AdministratorSelenium;
import Paginas.CarPage;
import Paginas.LoginPage;
import Paginas.WishListPage;
import dev.failsafe.internal.util.Assert;


@Listeners(Listener.ListenerTestNG.class)
public class Car {
		
//	Se crean las instancias.
	CarPage PCar;
	AdministratorSelenium AAdministratorSelenium;
	
	//Datos.
	JSONObject carData;
	String jsonPath  = "src/json/wishList.json";
	String loginPath = "http://tutorialsninja.com/demo/index.php?route=account/login";
	
	@Parameters({"browserType", "driverPath", "evidencePath", "siteUrl"})
	@BeforeClass
	public void beforeClass(String browserType, String driverPath, String evidencePath, String siteUrl) throws IOException, ParseException {

//		Se crean las instancias.
		PCar                   = new CarPage( browserType, driverPath, evidencePath );
		AAdministratorSelenium = new AdministratorSelenium( PCar.getDriver(), evidencePath );
		
//		Read JSON.
		carData = PCar.readJSON( jsonPath );
		
//		Do Login.
		PCar.openUrl( loginPath );
		AAdministratorSelenium.doLogin( (String) carData.get("email"), (String) carData.get("password") );
		
//	    Go to main page.
		PCar.openUrl( siteUrl );
			
	}
	
	@BeforeMethod
	public void beforeTest() {
		


	}
	
	@AfterClass
	public void afterClass() {
		
		
		
	}
	
	@AfterMethod
	public void afterTest() {

		
		
	}
	
	@Test
	public void TestCaseCheckOut() throws IOException, InterruptedException {
				
		PCar.navigateToMp3Players();
		PCar.navigateToShowAll();
		PCar.addtoCar();
		PCar.clickCheckOut();
		PCar.clickAdressButton();
		PCar.clickAgree();
		PCar.clickContinueButton();
		PCar.validateCheckOut();
		Thread.sleep(2000);
		
	}
	
}
