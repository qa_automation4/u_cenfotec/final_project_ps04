package Pruebas;

import java.io.IOException;

import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import Administrator.AdministratorSelenium;
import Paginas.HomePage;


@Listeners(Listener.ListenerTestNG.class)
public class Home {
		
//	Se crean las instancias.
	HomePage PHomePage;
	AdministratorSelenium AAdministratorSelenium;
	
//	Datos.
	JSONObject homeData;
	String jsonPath  = "src/json/home.json";
	String loginPath = "http://tutorialsninja.com/demo/index.php?route=account/login";

	
	
	@Parameters({"browserType", "driverPath", "evidencePath", "siteUrl"})
	@BeforeClass
	public void beforeClass( String browserType, String driverPath, String evidencePath, String siteUrl ) throws IOException, ParseException, InterruptedException {
		
//		Se crean las instancias.
		PHomePage              = new HomePage( browserType, driverPath, evidencePath );
		AAdministratorSelenium = new AdministratorSelenium( PHomePage.getDriver(), evidencePath );

//		Read JSON.
		homeData = PHomePage.readJSON( jsonPath );
		
//		Do LogOut.
		AAdministratorSelenium.doLogOut();
		
	}
	
	
	@Parameters({"siteUrl"})
	@BeforeMethod
	public void beforeTest( String siteUrl ) {
		
//		Se navega al site.
		PHomePage.openUrl( siteUrl );
		
	}
	
	@AfterClass
	public void afterClass() {
		

		
	}
	
	@AfterMethod
	public void afterTest() {
		

		
	}
	
	@Test
	public void selectEuroCurrency() throws IOException, InterruptedException {
		
		PHomePage.clickCurrencySelect();
		PHomePage.chooseCurrency( (String) homeData.get("euro") );
		PHomePage.validationCurrencyItem( (String) homeData.get("euroCurrency") );
		Thread.sleep(2000);
		
	}
	
	@Test
	public void selectPoundCurrency() throws IOException, InterruptedException {
		
		PHomePage.clickCurrencySelect();
		PHomePage.chooseCurrency( (String) homeData.get("pound") );
		PHomePage.validationCurrencyItem( (String) homeData.get("poundCurrency") );
		Thread.sleep(2000);
		
	}
	
	@Test
	public void selectDolarCurrency() throws IOException, InterruptedException {
		
		PHomePage.clickCurrencySelect();
		PHomePage.chooseCurrency( (String) homeData.get("dolar") );
		PHomePage.validationCurrencyItem( (String) homeData.get("dolarCurrency") );
		Thread.sleep(2000);
		
	}
	
	@Test
	public void goToRegisterView() throws InterruptedException, IOException {
		
		PHomePage.clickMyAccount();
		PHomePage.clickRegisterOption();
		PHomePage.validationRegisterTittleView();
		Thread.sleep(2000);
	}
	
	@Test
	public void goToLoginView() throws InterruptedException, IOException {
		
		PHomePage.clickMyAccount();
		PHomePage.clickLoginOption();
		PHomePage.validationLoginTittleView();
		Thread.sleep(2000);
		
	}
	
	@Test
	public void goToContactView() throws InterruptedException, IOException {
		
		PHomePage.clickContactIcon();
		PHomePage.validationContactTittleView();
		Thread.sleep(2000);
		
	}
	
	@Test
	public void addItemToCart() throws InterruptedException, IOException {
		
		PHomePage.enterItem( (String) homeData.get("itemToSearch") );
		PHomePage.clickSearchItem();
		PHomePage.clickAddToCart( (String) homeData.get("itemToSearch") );
		PHomePage.validationSuccessMSAddToCart( (String) homeData.get("itemToSearch") );
		Thread.sleep(2000);
		
	}
	
	@Test
	public void addItemToWishList() throws InterruptedException, IOException {
		
		PHomePage.openUrl(loginPath);
		AAdministratorSelenium.doLogin( (String) homeData.get("email"), (String) homeData.get("password") );
		
		PHomePage.enterItem( (String) homeData.get("itemToSearch") );
		PHomePage.clickSearchItem();
		PHomePage.clickAddToWishList( (String) homeData.get("itemToSearch") );
		PHomePage.validationSuccessMSAddToWishList( (String) homeData.get("itemToSearch") );
		Thread.sleep(2000);
		
	}
	
}
