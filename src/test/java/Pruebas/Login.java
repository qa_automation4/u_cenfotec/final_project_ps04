package Pruebas;

import java.io.IOException;

import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import Paginas.LoginPage;


@Listeners(Listener.ListenerTestNG.class)
public class Login {
	
//	Se crean las instancias.
	LoginPage PLoginPage;
	
//	Datos.
	JSONObject loginData;
	String jsonPath = "src/json/login.json";
	
	
	@Parameters({"browserType", "driverPath", "evidencePath", "siteUrl"})
	@BeforeClass
	public void beforeClass( String browserType, String driverPath, String evidencePath, String siteUrl ) throws IOException, ParseException {
		
//		Se crean las instancias.
		PLoginPage = new LoginPage( browserType, driverPath, evidencePath );
		
//		Read JSON.
		loginData = PLoginPage.readJSON( jsonPath );
		
	}
	

	@Parameters({"siteUrl"})
	@BeforeMethod
	public void beforeTest( String siteUrl ) {
		
//		Se navega al site.
		PLoginPage.openUrl( siteUrl );
		
	}
	
	@AfterClass
	public void afterClass() {
		

		
	}
	
	@AfterMethod
	public void afterTest() {
		
		
		
	}
	
	
	@Test
	public void TestCaseIncorrectLogin() throws IOException, InterruptedException {
		
		PLoginPage.enterEmail( (String) loginData.get("wrongEmail") );
		PLoginPage.enterPassword( (String) loginData.get("password") );
		PLoginPage.clickBtnLogin();
		PLoginPage.validationWarningMessage();
		Thread.sleep(2000);

	}
	
	@Test
	public void TestCaseCorrectLogin() throws IOException, InterruptedException {
		
		PLoginPage.enterEmail( (String) loginData.get("email") );
		PLoginPage.enterPassword( (String) loginData.get("password") );
		PLoginPage.clickBtnLogin();
		PLoginPage.validationMyAccountTittle();
		Thread.sleep(2000);

	}

}
