package Pruebas;

import java.io.IOException;

import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import Administrator.AdministratorSelenium;
import Paginas.UserAccountPage;


@Listeners(Listener.ListenerTestNG.class)
public class UserAccount {
	
//	Se crean las instancias.
	UserAccountPage PUserAccountPage;
	AdministratorSelenium AAdministratorSelenium;
	
//	Datos.
	JSONObject userAccountData;
	String jsonPath = "src/json/userAccount.json";
	String email;
	String password;
	
	
	String accountPath = "http://tutorialsninja.com/demo/index.php?route=account/account";
	String loginPath   = "http://tutorialsninja.com/demo/index.php?route=account/login";
	


	@Parameters({"browserType", "driverPath", "evidencePath", "siteUrl"})
	@BeforeClass
	public void beforeClass( String browserType, String driverPath, String evidencePath, String siteUrl ) throws IOException, ParseException, InterruptedException {
		
//		Se crean las instancias.
		PUserAccountPage       = new UserAccountPage( browserType, driverPath, evidencePath );
		AAdministratorSelenium = new AdministratorSelenium( PUserAccountPage.getDriver(), evidencePath );
		
//		Read JSON.
		userAccountData = PUserAccountPage.readJSON( jsonPath );
		
//		Go to Account.
		PUserAccountPage.openUrl( accountPath );
		
//		Do LogOut.
		AAdministratorSelenium.doLogOut();
		
//		Do Login.
		PUserAccountPage.openUrl( loginPath );
		AAdministratorSelenium.doLogin( (String) userAccountData.get("email"), (String) userAccountData.get("password") );
		
	}
	
	@Parameters({"browserType", "driverPath", "evidencePath", "siteUrl"})
	@BeforeMethod
	public void beforeTest( String browserType, String driverPath, String evidencePath, String siteUrl ) {
		
//		Se navega al site.
		PUserAccountPage.openUrl( siteUrl );
		
		
	}
	
	@AfterClass
	public void afterClass() {
		
		//PUserAccountPage.closeDriver();
		
	}
	
	@AfterMethod
	public void afterTest() {
		
		
		
	}
	
	@Test
	public void TestCaseEditUserData() throws InterruptedException, IOException {
		
		PUserAccountPage.clickBtnEditAccount();
		
		PUserAccountPage.enterFirstName( (String) userAccountData.get("firstName") );
		PUserAccountPage.enterLastName( (String) userAccountData.get("lastName") );
		PUserAccountPage.enterUserEmail( (String) userAccountData.get("email") );
		PUserAccountPage.enterUserTelephone( (String) userAccountData.get("telephone") );
		Thread.sleep(2000);
		PUserAccountPage.clickBtnContinue();
		PUserAccountPage.validationSuccessMessage();
		Thread.sleep(2000);

	}
	
	@Test
	public void TestCaseChangePassword() throws InterruptedException, IOException {
		
		PUserAccountPage.clickBtnPassword();
		PUserAccountPage.enterChangePassword( (String) userAccountData.get("changePassword") );
		PUserAccountPage.enterChangeConfirmPassword( (String) userAccountData.get("changePasswordConfirm") );
		Thread.sleep(2000);
		PUserAccountPage.clickBtnContinue();
		PUserAccountPage.validationSuccessChPMessage();
		Thread.sleep(2000);

	}
	
	@Test
	public void TestCaseAddAddressBook() throws InterruptedException, IOException {
		
		PUserAccountPage.clickBtnAddressBook();
		PUserAccountPage.clickBtnNewAddress();
		PUserAccountPage.enterAddressFirstName( (String) userAccountData.get("addressFirstName") );
		PUserAccountPage.enterAddressLastName( (String) userAccountData.get("addressLastName") );
		PUserAccountPage.enterAddressCompanyName( (String) userAccountData.get("addressCompanyName") );
		PUserAccountPage.enterAddressAddress1( (String) userAccountData.get("addressAddress1") );
		PUserAccountPage.enterAddressAddress2( (String) userAccountData.get("addressAddress2") );
		PUserAccountPage.enterAddressCity( (String) userAccountData.get("addressCity") );
		PUserAccountPage.enterAddressPostCode( (String) userAccountData.get("addressPostCode") );
		PUserAccountPage.selectAddressCountry( (String) userAccountData.get("addressCountry") );
		PUserAccountPage.selectAddressRegionState( (String) userAccountData.get("addressRegionState") );
		PUserAccountPage.chooseDefaultAddress("1");
		Thread.sleep(2000);
		PUserAccountPage.clickBtnContinue();
		PUserAccountPage.validationSuccessAddressAddedMessage();
		Thread.sleep(2000);

	}
	
	@Test
	public void TestCaseRegisterAffiliateAccountCheque() throws InterruptedException, IOException {
		

		PUserAccountPage.affiliateAccountLinkIsVisibility();
				
		PUserAccountPage.enterAffiliateCompany( (String) userAccountData.get("affiliateCompany") );
		PUserAccountPage.enterAffiliateWebSite( (String) userAccountData.get("affiliateWebSite") );
		PUserAccountPage.enterAffiliateTaxID( (String) userAccountData.get("affiliateTaxID") );
		PUserAccountPage.enterAffiliateChequePayeeName( (String) userAccountData.get("affiliateChequePayeeName") );
		PUserAccountPage.chooseAffiliatePaymentMethod( (String) userAccountData.get("affiliatePaymentMethod") );
		Thread.sleep(2000);
		PUserAccountPage.clickBtnContinue();
		PUserAccountPage.validationSuccessAffiliateAccountMessage();
		Thread.sleep(2000);

	}
	
	@Test
	public void TestCaseSubscribeNewsletter() throws InterruptedException, IOException {
		
		PUserAccountPage.clickLiSubscribeNewsletter();		
		PUserAccountPage.chooseNewsLetterSubscribeYes();
		Thread.sleep(2000);
		PUserAccountPage.clickBtnContinue();
		PUserAccountPage.validationSubscribeNewsletterMessage();
		Thread.sleep(2000);

	}
	
	@Test
	public void TestCaseLogout() throws InterruptedException, IOException {
		
		PUserAccountPage.clickBtnLogout();
		PUserAccountPage.validationLogoutTittle();
		Thread.sleep(2000);

	}
	
}
