package Pruebas;

import java.io.IOException;

import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import Administrator.AdministratorSelenium;
import Paginas.RegisterPage;


@Listeners(Listener.ListenerTestNG.class)
public class Register {
		
//	Se crean las instancias.
	RegisterPage PRegisterPage;
	AdministratorSelenium AAdministratorSelenium;
	

	
	
//	Datos.
	JSONObject registerData;
	String jsonPath = "src/json/register.json";
	String emailToRegister;
	
	
	@Parameters({"browserType", "driverPath", "evidencePath", "siteUrl"})
	@BeforeClass
	public void beforeClass( String browserType, String driverPath, String evidencePath, String siteUrl ) throws IOException, ParseException {

//		Se crean las instancias.
		PRegisterPage          = new RegisterPage( browserType, driverPath, evidencePath );
		AAdministratorSelenium = new AdministratorSelenium( PRegisterPage.getDriver(), evidencePath );
		
//		Se navega al site de emails.			
		AAdministratorSelenium.openUrlTempEmail();
		
//		Se obtiene el email aleatorio desde otro sitio web especializado en generar email de manera randow.
		emailToRegister = AAdministratorSelenium.getEmail();
		
//		Read JSON.
		registerData = PRegisterPage.readJSON( jsonPath );
		
	}
	
	@Parameters({"siteUrl"})
	@BeforeMethod
	public void beforeTest( String siteUrl ) throws IOException {
		
//		Se navega al site.			
		PRegisterPage.openUrl(siteUrl);
		
	}
	
	@AfterClass
	public void afterClass() {
		

		
	}
	
	@AfterMethod
	public void afterTest() {
		
		
		
	}
	
	@Test
	public void TestCaseRegistrarUsuario() throws IOException, InterruptedException {
		
		PRegisterPage.enterFirstName( (String) registerData.get("firstName") );
		PRegisterPage.enterLastName( (String) registerData.get("lastName") );
		PRegisterPage.enterEmail( emailToRegister );
		PRegisterPage.enterTelefono( (String) registerData.get("telephone") );
		PRegisterPage.enterPassword( (String) registerData.get("password") );
		PRegisterPage.enterPasswordConfirm( (String) registerData.get("passwordConfirm") );
		PRegisterPage.chooseNewsSubscribe( (String) registerData.get("newsSubscribe") );
		PRegisterPage.checkPrivacy();
		Thread.sleep(2000);
		PRegisterPage.clickBtnContinue();
		PRegisterPage.validationAccountCreated();
		Thread.sleep(2000);
		PRegisterPage.clickBtnLogout();
		
	}
	
	@Test
	public void TestCaseRegistrarUsuarioSinPrivacy() throws IOException, InterruptedException {
		
		PRegisterPage.enterFirstName( (String) registerData.get("firstName") );
		PRegisterPage.enterLastName( (String) registerData.get("lastName") );
		PRegisterPage.enterEmail( emailToRegister );
		PRegisterPage.enterTelefono( (String) registerData.get("telephone") );
		PRegisterPage.enterPassword( (String) registerData.get("password") );
		PRegisterPage.enterPasswordConfirm( (String) registerData.get("passwordConfirm") );
		PRegisterPage.chooseNewsSubscribe( (String) registerData.get("newsSubscribe") );
		Thread.sleep(2000);
		PRegisterPage.clickBtnContinue();
		PRegisterPage.validationPrivacyErrorMessage();
		Thread.sleep(2000);
		
	}
	
	@Test
	public void TestCaseRegistrarUsuarioCamposVacios() throws IOException, InterruptedException {
		
		PRegisterPage.enterFirstName( (String) registerData.get("firstName") );
		PRegisterPage.enterLastName( (String) registerData.get("lastName") );
		PRegisterPage.enterEmail( emailToRegister );
		PRegisterPage.enterPassword( (String) registerData.get("password") );
		PRegisterPage.enterPasswordConfirm( (String) registerData.get("passwordConfirm") );
		PRegisterPage.chooseNewsSubscribe( (String) registerData.get("newsSubscribe") );
		Thread.sleep(2000);
		PRegisterPage.clickBtnContinue();
		PRegisterPage.validationTelephoneErrorMessage();
		Thread.sleep(2000);
		
	}
	
}
