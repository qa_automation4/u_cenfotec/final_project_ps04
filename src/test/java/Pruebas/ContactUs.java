package Pruebas;

import java.io.IOException;

import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import Paginas.ContactUsPage;


@Listeners(Listener.ListenerTestNG.class)
public class ContactUs {
	
//	Se crean las instancias.
	ContactUsPage PContactUsPage;
	
//	Datos.
	JSONObject contactData;
	String jsonPath = "src/json/contact.json";

	
	
	@Parameters({"browserType", "driverPath", "evidencePath", "siteUrl"})
	@BeforeMethod
	public void beforeTest( String browserType, String driverPath, String evidencePath, String siteUrl ) throws IOException, ParseException {
		
//		Se crean las instancias.
		PContactUsPage = new ContactUsPage( browserType, driverPath, evidencePath );
		
//		Se navega al site.
		PContactUsPage.openUrl( siteUrl );
		
//		Read JSON.
		contactData = PContactUsPage.readJSON( jsonPath );
		
	}
	
	@AfterClass
	public void afterClass() {
		

		
	}
	
	@AfterMethod
	public void afterTest() {
		
		
		
	}
	
	@Test
	public void TestCaseSendContactForm() throws InterruptedException, IOException {
		
		PContactUsPage.enterName( (String) contactData.get("name") );
		PContactUsPage.enterEmail( (String) contactData.get("email") );
		PContactUsPage.enterEnquiry( (String) contactData.get("enquiry") );
		Thread.sleep(2000);
		PContactUsPage.clickBtnSubmit();

	}
	
}
