package Pruebas;


import java.io.IOException;

import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import Administrator.AdministratorSelenium;
import Paginas.WishListPage;


@Listeners(Listener.ListenerTestNG.class)
public class WishList {
		
//	Se crean las instancias.
	WishListPage PWishList;
	AdministratorSelenium AAdministratorSelenium;
	
//	Datos.
	JSONObject wishListData;
	String jsonPath  = "src/json/wishList.json";
	String loginPath = "http://tutorialsninja.com/demo/index.php?route=account/login";
	
	
	
	@Parameters({"browserType", "driverPath", "evidencePath", "siteUrl"})
	@BeforeClass
	public void beforeClass( String browserType, String driverPath, String evidencePath, String siteUrl ) throws IOException, ParseException, InterruptedException {

//		Se crean las instancias.
		PWishList              = new WishListPage( browserType, driverPath, evidencePath );
		AAdministratorSelenium = new AdministratorSelenium( PWishList.getDriver(), evidencePath );
		
//		Read JSON.
		wishListData = PWishList.readJSON( jsonPath );
		
//	    Go to main page.
	    PWishList.openUrl( siteUrl );
			
	}
	
	@BeforeMethod
	public void beforeTest() {


		
	}
	
	@AfterClass
	public void afterClass() {
		
		AAdministratorSelenium.closeDriver();
		
	}
	
	@AfterMethod
	public void afterTest() {
		
		
		
	}
	
	@Test
	public void TestCaseAddItemToWishList() throws IOException, InterruptedException {
				
		PWishList.navigateToCameras();
		PWishList.clickAddToWishList();
		PWishList.validateItemAddedToWishList();
		Thread.sleep(2000);
		
	}
	
	@Test
	public void DeleteItemToWishList() throws IOException, InterruptedException {
					
		PWishList.navigateToCameras();
		PWishList.clickAddToWishList();
		PWishList.navigateToWishList();
		PWishList.deleteFromWishList();
		PWishList.validateItemRemovedFromWishList();
		Thread.sleep(2000);
		
	}
	
}
