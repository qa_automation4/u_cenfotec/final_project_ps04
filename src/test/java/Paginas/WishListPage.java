package Paginas;

import static org.testng.Assert.assertEquals;

import java.io.IOException;

import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import Adaptador.AdaptadorSelenium;




public class WishListPage {
		
	By addWishListButton    = By.xpath("(//button[@type='button' and @data-original-title='Add to Wish List'])[2]");
	By byNavigateCameras    = By.xpath("//a[@href='http://tutorialsninja.com/demo/index.php?route=product/category&path=33']");
	By byNavigateToWishList = By.xpath("//a[@href='http://tutorialsninja.com/demo/index.php?route=account/wishlist']");
	By byDeleteFromWishList = By.xpath("//a[@href='http://tutorialsninja.com/demo/index.php?route=account/wishlist&remove=31']");
	
	String evidencePath = "";
	
	private AdaptadorSelenium adaptador;
	

	
	public WishListPage(String browserType, String driverPath, String evidencePath) {
		
		adaptador         = AdaptadorSelenium.getAdaptador(browserType, driverPath);
		this.evidencePath = evidencePath;
		
	}

	
	
	public void openUrl(String url) {
		
		adaptador.openUrl(url);
		
	}
	
	public void closeDriver() {
		
		adaptador.closeDriver();
		
	}
	
	public void clickAddToWishList() {
		
		adaptador.toClick(addWishListButton);
	
	}
	public void deleteFromWishList() {
		
		adaptador.toClick(byDeleteFromWishList);
	
	}
	
	public void navigateToCameras() {
		
		adaptador.toClick(byNavigateCameras);
	
	}
	
	public void navigateToWishList() {
		
		adaptador.toClick(byNavigateToWishList);
	
	}

	public void validateItemAddedToWishList() throws IOException {
		
		adaptador.takeEvidence(this.evidencePath);
		assertEquals(adaptador.getText(By.cssSelector(".alert")),"Success: You have added Nikon D300 to your wish list!\n×");
		
	}
	public void validateItemRemovedFromWishList() throws IOException {
		
		adaptador.takeEvidence(this.evidencePath);
		assertEquals(adaptador.getText(By.cssSelector(".alert")),"Success: You have modified your wish list!\n×");
		
	}
	
	public JSONObject readJSON(String jsonName) throws IOException, ParseException {
		
		return adaptador.readJSON(jsonName);
		
	}
	
	public WebDriver getDriver() {
		
		return adaptador.getDriver();
	
	}
	
}
