package Paginas;

import static org.testng.Assert.assertEquals;

import java.io.IOException;

import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import Adaptador.AdaptadorSelenium;



public class CarPage {
	
	By Navigate             = By.xpath("//a[@href='http://tutorialsninja.com/demo/index.php?route=product/category&path=34']");
	By byNavigateMp3Players = By.linkText("MP3 Players");
	By byNavigateMp3ShowAll = By.linkText("Show All MP3 Players");
	By byAddtoCart          = By.xpath("(//div[@class='button-group']//button//i[@class='fa fa-shopping-cart']//ancestor::button)[2]");
	By byCheckOuttButton    = By.xpath("//span[normalize-space()='Checkout']");
	By byAdressButton       = By.id("button-payment-address");
	By byAgree              = By.xpath("//input[@name='agree']");
	By byContinueButton     = By.id("button-payment-method");
	
	String evidencePath = "";
	
	
	private AdaptadorSelenium adaptador;
	
	

	public CarPage( String browserType, String driverPath, String evidencePath ) {
		
		adaptador         = AdaptadorSelenium.getAdaptador( browserType, driverPath );
		this.evidencePath = evidencePath;
		
	}

	

	public void openUrl( String url ) {
		
		adaptador.openUrl( url );
		
	}
	
	public void closeDriver() {
		
		adaptador.closeDriver();
		
	}
	
	

	public void navigateToMp3Players() {
		
		adaptador.toClick( byNavigateMp3Players );
	
	}
	public void navigateToShowAll() {
		
		adaptador.toClick( byNavigateMp3ShowAll );
		
	}	
	
	public void addtoCar() {
		
		adaptador.toClick( byAddtoCart );
		
	}
	public void clickCheckOut() {
		
		adaptador.toClick( byCheckOuttButton );
		
	}	
	public void clickAdressButton() {
		
		adaptador.toClick( byAdressButton );
		
	}	
	public void clickAgree() {
		
		adaptador.toClick( byAgree );
		
	}	
	public void clickContinueButton() {
	
	adaptador.toClick( byContinueButton );
	
	}	
	
	public void validateCheckOut() throws IOException {
		
		assertEquals(adaptador.getText( By.cssSelector(".alert")),"Warning: No Payment options are available. Please contact us for assistance!" );
		adaptador.takeEvidence( this.evidencePath );
	   
			
	}
	
	public WebDriver getDriver() {
		
		return adaptador.getDriver();
	
	}
	public JSONObject readJSON( String jsonName ) throws IOException, ParseException {
		
		return adaptador.readJSON( jsonName );
		
	}

}
