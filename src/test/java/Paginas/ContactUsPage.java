package Paginas;


import static org.testng.Assert.assertEquals;

import java.io.IOException;

import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import Adaptador.AdaptadorSelenium;



public class ContactUsPage {

	@FindBy(id="input-name")
	WebElement inputName;
	
	@FindBy(id="input-email")
	WebElement inputEmail;
	
	@FindBy(id="input-enquiry")
	WebElement inputEnquiry;
	
	@FindBy(xpath="//input[@type='submit' and @value='Submit']")
	WebElement btnSubmit;
	
	By byContactUsTittle = By.xpath("//h1");
	
	String evidencePath = "";
	
	private AdaptadorSelenium adaptador;

	
	
	public ContactUsPage( String browserType, String driverPath, String evidencePath ) {
		
		adaptador         = AdaptadorSelenium.getAdaptador( browserType, driverPath );
		PageFactory.initElements( adaptador.getDriver(), this );
		this.evidencePath = evidencePath;
		
	}
	
	
	
	public void openUrl( String url ) {
		
		adaptador.openUrl( url );
		
	}
	
	public void closeDriver() {
		
		adaptador.closeDriver();
		
	}
	
	public void enterName( String name ) {
		
		adaptador.inputTextByElement( inputName, name );
		
	}
	
	public void enterEmail( String email ) {
		
		adaptador.inputTextByElement( inputEmail, email );
		
	}
	
	public void enterEnquiry( String enquiry ) {
		
		adaptador.inputTextByElement( inputEnquiry, enquiry );
		
	}
	
	public void clickBtnSubmit() throws IOException {
		
		adaptador.takeEvidence( this.evidencePath );
		adaptador.toClickByElement( btnSubmit );
		
	}
	
	public void validationContactUsTittleMessage() throws IOException {
		
		adaptador.takeEvidence( this.evidencePath );
		assertEquals(adaptador.getText( byContactUsTittle ), "Contact Us" );
		
	}
	
	public JSONObject readJSON( String jsonName ) throws IOException, ParseException {
		
		return adaptador.readJSON( jsonName );
		
	}
	
}
