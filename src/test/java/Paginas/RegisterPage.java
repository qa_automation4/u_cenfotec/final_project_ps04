package Paginas;

import static org.testng.Assert.assertEquals;

import java.io.IOException;

import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import Adaptador.AdaptadorSelenium;



public class RegisterPage {
	
	@FindBy(id="input-firstname")
	WebElement inputFirstName;
	
	@FindBy(id="input-lastname")
	WebElement inputLastname;
	
	@FindBy(id="input-email")
	WebElement inputEmail ;
	
	@FindBy(id="input-telephone")
	WebElement inputTelephone;
	
	@FindBy(id="input-password")
	WebElement inputPassword;
	
	@FindBy(id="input-confirm")
	WebElement inputPasswordConfirm;
	
	@FindBy(xpath="//input[@name='agree' and @value='1']")
	WebElement checkBoxPolicy;
	
	@FindBy(xpath="//input[@type='submit' and @value='Continue']")
	WebElement btnContinue ;

	By byRadioNewsSubscribe;
	
	By byAccountCreatedTitle    = By.xpath("//div[@id='content']//h1");
	By byErrorPrivacyMessage    = By.xpath("//div[@class='alert alert-danger alert-dismissible']");
	By byErrorTelephoneMessage  = By.xpath("//input[@id='input-telephone']//following-sibling::div");
	By byBtnLogout              = By.xpath("//a[@class='list-group-item'][normalize-space()='Logout']");

	
	String xpathRadioNewsSubscribeOption = "//input[@name='newsletter' and @value='optionToSelect']";
	String evidencePath                  = "";
	
	private AdaptadorSelenium adaptador;
	
	
	
	public RegisterPage( String browserType, String driverPath, String evidencePath ) {
		
		adaptador         = AdaptadorSelenium.getAdaptador( browserType, driverPath );
		PageFactory.initElements( adaptador.getDriver(), this );
		this.evidencePath = evidencePath;
		
	}
	
	
	
	public void openUrl( String url ) {
		
		adaptador.openUrl( url );
		
	}
	
	public void closeDriver() {
		
		adaptador.closeDriver();
		
	}
	
	public void enterFirstName( String firstName ) {
		
		adaptador.inputTextByElement( inputFirstName, firstName );
		
	}
	
	public void enterLastName( String lastName ) {
		
		adaptador.inputTextByElement( inputLastname, lastName );
		
	}
	
	public void enterEmail( String email ) {
		
		adaptador.inputTextByElement( inputEmail, email );
		
	}
	
	public void enterTelefono( String telephone ) {
		
		adaptador.inputTextByElement( inputTelephone, telephone );
		
	}
	
	public void enterPassword( String password ) {
		
		adaptador.inputTextByElement( inputPassword, password );
		
	}
	
	public void enterPasswordConfirm( String passwordConfirm ) {
		
		adaptador.inputTextByElement( inputPasswordConfirm, passwordConfirm );
		
	}
	
	public void chooseNewsSubscribe( String value ) {
		
		String xpath         = xpathRadioNewsSubscribeOption.replace( "optionToSelect", value );
		byRadioNewsSubscribe = By.xpath( xpath );
		
		adaptador.toClick( byRadioNewsSubscribe );
		
	}
	
	public void checkPrivacy() {
		
		adaptador.toClickByElement( checkBoxPolicy );
		
	}
	
	public void clickBtnContinue() {
		
		adaptador.toClickByElement( btnContinue );
		
	}
	
	public void clickBtnLogout() {
		
		adaptador.toClick( byBtnLogout );
		
	}
	
	public void validationAccountCreated() throws IOException {
		
		adaptador.takeEvidence( this.evidencePath );
		assertEquals(adaptador.getText( byAccountCreatedTitle ), "Your Account Has Been Created!" );
		
	}
	
	public void validationPrivacyErrorMessage() throws IOException {
		
		adaptador.takeEvidence( this.evidencePath );
		assertEquals(adaptador.getText( byErrorPrivacyMessage ), "Warning: You must agree to the Privacy Policy!" );
		
	}
	
	public void validationTelephoneErrorMessage() throws IOException {
		
		adaptador.takeEvidence( this.evidencePath );
		assertEquals(adaptador.getText( byErrorTelephoneMessage ), "Telephone must be between 3 and 32 characters!" );
		
	}
	
	public JSONObject readJSON( String jsonName ) throws IOException, ParseException {
		
		return adaptador.readJSON( jsonName );
		
	}
	
	public WebDriver getDriver() {
		
		return adaptador.getDriver();
		
	}
	
}
