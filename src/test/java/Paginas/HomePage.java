package Paginas;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

import java.io.IOException;

import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import Adaptador.AdaptadorSelenium;



public class HomePage {
	
	@FindBy(xpath="//input[@name='search']")
	WebElement inputSearch;
	
	@FindBy(xpath="//div[@id='search']//span//button[@type='button']")
	WebElement btnSearch;
	
	@FindBy(xpath="//button[@class='btn btn-link dropdown-toggle' and @data-toggle='dropdown']")
	WebElement btnCurrencySelect;
	
	@FindBy(xpath="//a[@title='My Account']")
	WebElement btnMyAccount;
	
	@FindBy(xpath="//a[@href='http://tutorialsninja.com/demo/index.php?route=information/contact']")
	WebElement btnContactIcon;
	
	By byCurrencyItems      = By.xpath("//p[@class='price']");
	By byChooseCurrency;
	By byRegisterOption     = By.xpath("//a[@href='http://tutorialsninja.com/demo/index.php?route=account/register']");
	By byLoginOption        = By.xpath("//a[@href='http://tutorialsninja.com/demo/index.php?route=account/login']");
	By byRegisterTittleView = By.xpath("//h1[normalize-space()='Register Account']");
	By byLoginTittleView    = By.xpath("//h2[normalize-space()='Returning Customer']");
	By byContactTittleView  = By.xpath("//h1[normalize-space()='Contact Us']");
	By byItem;
	By byBtnAddToCart       = By.xpath("(//div[@class='button-group']//button//i[@class='fa fa-shopping-cart']//ancestor::button)[1]");
	By byBtnAddToWishList   = By.xpath("(//div[@class='button-group']//button//i[@class='fa fa-heart']//ancestor::button)[1]");
	By bySuccessMessage;
	
	String xpathCurrency       = "//ul[@class='dropdown-menu']//li//button[@name='currencyName']";
	String xpathSuccessMessage = "//div[@class='alert alert-success alert-dismissible']//a[contains(text(), 'itemToSearch')]//ancestor::div";
	String xpathItem           = "(//h4/a[contains(text(),'ItemToSearch')])[1]";
	String evidencePath        = "";

	private AdaptadorSelenium adaptador;

	
	
	public HomePage( String browserType, String driverPath, String evidencePath ) {
		
		adaptador         = AdaptadorSelenium.getAdaptador( browserType, driverPath );
		PageFactory.initElements( adaptador.getDriver(), this );
		this.evidencePath = evidencePath;
		
	}
	
	
	
	public void openUrl( String url ) {
		
		adaptador.openUrl( url );
		
	}
	
	public void closeDriver() {
		
		adaptador.closeDriver();
		
	}
	
	public void enterItem( String itemName ) {
		
		adaptador.inputTextByElement( inputSearch, itemName );
		
	}
	
	public void clickCurrencySelect() {
		
		adaptador.toClickByElement( btnCurrencySelect );
		
	}
	
	public void clickMyAccount() {
		
		adaptador.toClickByElement( btnMyAccount );
		
	}
	
	public void clickRegisterOption() {
		
		adaptador.toClick( byRegisterOption );
		
	}
	
	public void clickLoginOption() {
		
		adaptador.toClick( byLoginOption );
		
	}
	
	public void clickContactIcon() {
		
		adaptador.toClickByElement( btnContactIcon );
		
	}
	
	public void clickSearchItem() {
		
		adaptador.toClickByElement( btnSearch );
		
	}
	
	public void clickAddToCart( String itemToSearch ) {
		
		String xpath = xpathItem.replace( "ItemToSearch", itemToSearch );
		byItem       = By.xpath( xpath );
		
		adaptador.getExistingElement( byItem );
		adaptador.toClick( byBtnAddToCart );
		
	}
	
	public void clickAddToWishList( String itemToSearch ) {
		
		String xpath = xpathItem.replace( "ItemToSearch", itemToSearch );
		byItem       = By.xpath(xpath);
		
		adaptador.getExistingElement( byItem );
		adaptador.toClick( byBtnAddToWishList );
		
	}
	
	public void chooseCurrency( String currency ) {
		
		String xpath     = xpathCurrency.replace( "currencyName", currency );
		byChooseCurrency = By.xpath( xpath );
		
		adaptador.toClick( byChooseCurrency );
		
	}
	
	public void validationCurrencyItem( String currency ) throws IOException {
		
		adaptador.takeEvidence( this.evidencePath );
		
		assertTrue(adaptador.getText( byCurrencyItems ).contains( currency ));
		
	}
	
	public void validationRegisterTittleView() throws IOException {
		
		adaptador.takeEvidence( this.evidencePath );
		
		assertEquals(adaptador.getText( byRegisterTittleView ), "Register Account");
		
	}
	
	public void validationLoginTittleView() throws IOException {
		
		adaptador.takeEvidence( this.evidencePath );
		
		assertEquals(adaptador.getText( byLoginTittleView ), "Returning Customer");
		
	}
	
	public void validationContactTittleView() throws IOException {
		
		adaptador.takeEvidence( this.evidencePath );
		
		assertEquals(adaptador.getText( byContactTittleView ), "Contact Us");
		
	}
	
	public void validationSuccessMSAddToCart( String item ) throws IOException {
		
		adaptador.takeEvidence(this.evidencePath);
		
		String xpath     = xpathSuccessMessage.replace( "itemToSearch", item );
		bySuccessMessage = By.xpath( xpath );
		
		assertTrue(adaptador.getText( bySuccessMessage ).contains( "Success: You have added " + item + " to your shopping cart!" ));
		
	}
	
	public void validationSuccessMSAddToWishList( String item ) throws IOException {
		
		adaptador.takeEvidence( this.evidencePath );
		
		String xpath     = xpathSuccessMessage.replace( "itemToSearch", item );
		bySuccessMessage = By.xpath( xpath );
		
		assertTrue(adaptador.getText( bySuccessMessage ).contains( "Success: You have added " + item + " to your wish list!" ));
		
	}
	
	public JSONObject readJSON( String jsonName ) throws IOException, ParseException {
		
		return adaptador.readJSON( jsonName );
		
	}
	
	public WebDriver getDriver() {
		
		return adaptador.getDriver();
		
	}
	
}
