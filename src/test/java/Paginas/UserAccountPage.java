package Paginas;

import static org.testng.Assert.assertEquals;

import java.io.IOException;

import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import Adaptador.AdaptadorSelenium;



public class UserAccountPage {
		
	@FindBy(xpath="//a[@href=\"http://tutorialsninja.com/demo/index.php?route=account/edit\"]")
	WebElement btnEditAccount ;
	
	@FindBy(xpath="//a[@href=\"http://tutorialsninja.com/demo/index.php?route=account/password\"]")
	WebElement btnPassword  ;
	
	@FindBy(xpath="//a[@href=\"http://tutorialsninja.com/demo/index.php?route=account/address\"]")
	WebElement btnAddressBook ;
	
	@FindBy(xpath="//a[@class='list-group-item'][normalize-space()='Logout']")
	WebElement btnLogout ;
	
	By byLogoutTittle               = By.xpath("//h1[normalize-space()='Account Logout']");
	By byFirstName                  = By.id("input-firstname");
	By byLastName                   = By.id("input-lastname");
	By byUserEmail                  = By.id("input-email");
	By byUserTelephone              = By.id("input-telephone");
	By byChangePassword             = By.id("input-password");
	By byChangeConfirmPassword      = By.id("input-confirm");
	By byAddressFirstName           = By.id("input-firstname");
	By byAddressLastName            = By.id("input-lastname");
	By byAddressCompanyName         = By.id("input-company");
	By byAddressAddress1            = By.id("input-address-1");
	By byAddressAddress2            = By.id("input-address-2");
	By byAddressCity                = By.id("input-city");
	By byAddressPostCode            = By.id("input-postcode");
	By byAddressCountry             = By.id("input-country");
	By byAddressRegionState         = By.id("input-zone");
	By byAddressDefaultAddress;
	By byAffiliateCompany           = By.id("input-company");
	By byAffiliateWebSite           = By.id("input-website");
	By byAffiliateTaxID             = By.id("input-tax");
	By byAffiliateChequePayeeName   = By.id("input-cheque");
	By byAffiliateReadAgree         = By.name("agree");
	By byAffiliatePaymentMethod;
	By byNewsLetterSubscribeYes     = By.xpath("//input[@type='radio' and @name='newsletter']");
	By byLiRegisterAffiliateAccount = By.xpath("//a[@href=\"http://tutorialsninja.com/demo/index.php?route=account/affiliate/add\"]");
	By byLiEditAffiliateAccount     = By.xpath("//a[@href=\"http://tutorialsninja.com/demo/index.php?route=account/affiliate/edit\"]");
	By byLiSubscribeNewsletter      = By.xpath("//a[@href=\"http://tutorialsninja.com/demo/index.php?route=account/newsletter\"]");
	
	By byBtnContinue     = By.xpath("//input[@type='submit' and @value='Continue']");
	By byBtnNewAddress   = By.xpath("//a[@href=\"http://tutorialsninja.com/demo/index.php?route=account/address/add\"]");

	By bySuccessMessage = By.xpath("//div[@class='alert alert-success alert-dismissible']");
	
	String xpathDefaultValue  = "//input[@type='radio' and @value='defaultValue']";
	String xpathPaymentMethod = "//input[@type='radio' and @value='paymentMethod']";
	String evidencePath       = "";
	
	private AdaptadorSelenium adaptador;

	
	
	public UserAccountPage( String browserType, String driverPath, String evidencePath ) {
		
		adaptador         = AdaptadorSelenium.getAdaptador( browserType, driverPath );
		PageFactory.initElements( adaptador.getDriver(), this );
		this.evidencePath = evidencePath;
		
	}
	
	
	
	public void openUrl( String url ) {
		
		adaptador.openUrl( url );
		
	}
	
	public void closeDriver() {
		
		adaptador.closeDriver();
		
	}
	
	public void enterFirstName( String firstName ) {
		
		adaptador.inputText( byFirstName, firstName );
		
	}
	
	public void enterLastName( String lastName ) {
		
		adaptador.inputText( byLastName, lastName );
		
	}
	
	public void enterUserEmail( String userEmail ) {
		
		adaptador.inputText(byUserEmail, userEmail);
		
	}
	
	public void enterUserTelephone( String userTelephone ) {
		
		adaptador.inputText( byUserTelephone, userTelephone );
		
	}
	
	public void enterChangePassword( String changePassword ) {
		
		adaptador.inputText( byChangePassword, changePassword );
		
	}
	
	public void enterChangeConfirmPassword( String changeConfirmPassword ) {
		
		adaptador.inputText( byChangeConfirmPassword, changeConfirmPassword );
		
	}
	
	public void enterAddressFirstName( String addressFirstName ) {
		
		adaptador.inputText( byAddressFirstName, addressFirstName );
		
	}
	
	public void enterAddressLastName( String addressLastName ) {
		
		adaptador.inputText( byAddressLastName, addressLastName );
		
	}
	
	public void enterAddressCompanyName( String addressCompanyName ) {
		
		adaptador.inputText( byAddressCompanyName, addressCompanyName );
		
	}
	
	public void enterAddressAddress1( String addressAddress1 ) {
		
		adaptador.inputText( byAddressAddress1, addressAddress1 );
		
	}
	
	public void enterAddressAddress2( String addressAddress2 ) {
		
		adaptador.inputText( byAddressAddress2, addressAddress2 );
		
	}
	
	public void enterAddressCity( String addressCity ) {
		
		adaptador.inputText( byAddressCity, addressCity );
		
	}
	
	public void enterAddressPostCode( String addressPostCode ) {
		
		adaptador.inputText( byAddressPostCode, addressPostCode );
		
	}
	
	public void enterAffiliateCompany( String affiliateCompany ) {
		
		adaptador.inputText( byAffiliateCompany, affiliateCompany );
		
	}
	
	public void enterAffiliateWebSite( String affiliateWebSite ) {
		
		adaptador.inputText( byAffiliateWebSite, affiliateWebSite );
		
	}
	
	public void enterAffiliateTaxID( String affiliateTaxID ) {
		
		adaptador.inputText( byAffiliateTaxID, affiliateTaxID );
		
	}
	
	public void enterAffiliateChequePayeeName( String affiliateChequePayeeName ) {
		
		adaptador.inputText( byAffiliateChequePayeeName, affiliateChequePayeeName );
		
	}
	
	public void selectAddressCountry( String addressCountry ) {
		
		adaptador.select( byAddressCountry, "text", addressCountry );
		
	}
	
	public void selectAddressRegionState( String addressRegionState ) {
		
		adaptador.select( byAddressRegionState, "text", addressRegionState );
		
	}
	
	public void chooseDefaultAddress( String value ) {
		
		String xpath            = xpathDefaultValue.replace( "defaultValue", value );
		byAddressDefaultAddress = By.xpath( xpath );
		
		adaptador.toClick( byAddressDefaultAddress );
		
	}
	
	public void chooseAffiliatePaymentMethod( String value ) {
		
		String xpath             = xpathPaymentMethod.replace( "paymentMethod", value );
		byAffiliatePaymentMethod = By.xpath( xpath );
		
		adaptador.toClick( byAffiliatePaymentMethod );
		
	}
	
	public void chooseNewsLetterSubscribeYes() {
		
		adaptador.toClick( byNewsLetterSubscribeYes );
		
	}
	
	public void clickBtnContinue() throws IOException {
		
		adaptador.takeEvidence( this.evidencePath );
		adaptador.toClick( byBtnContinue );
		
	}
	
	public void clickBtnEditAccount() {
		
		adaptador.toClickByElement( btnEditAccount );
		
	}
	
	public void clickBtnPassword() {
		
		adaptador.toClickByElement( btnPassword );
		
	}
	
	public void clickBtnAddressBook() {
		
		adaptador.toClickByElement( btnAddressBook );
		
	}
	
	public void clickBtnNewAddress() {
		
		adaptador.toClick( byBtnNewAddress );
		
	}
	
	public void clickBtnLogout() {
		
		adaptador.toClickByElement( btnLogout );
		
	}
	
	public void clickLiRegisterAffiliateAccount() {
		
		adaptador.toClick( byLiRegisterAffiliateAccount );
		
	}
	
	public void clickLiEditAffiliateAccount() {
		
		adaptador.toClick( byLiEditAffiliateAccount );
		
	}
	
	public void clickInputAffiliateReadAgreet() {
		
		adaptador.toClick( byAffiliateReadAgree );
		
	}
	
	public void clickLiSubscribeNewsletter() {
		
		adaptador.toClick( byLiSubscribeNewsletter );
		
	}
	
	public void affiliateAccountLinkIsVisibility() {
		
		if (adaptador.elementIsDisplayed( byLiRegisterAffiliateAccount )) {
			
			clickLiRegisterAffiliateAccount();
			clickInputAffiliateReadAgreet();
			
		} else {
			
			clickLiEditAffiliateAccount();
			
		}

	}
	
	public void validationSuccessMessage() throws IOException {
		
		adaptador.takeEvidence( this.evidencePath );
		assertEquals(adaptador.getText( bySuccessMessage ), "Success: Your account has been successfully updated." );
		
	}
	
	public void validationSuccessChPMessage() throws IOException {
		
		adaptador.takeEvidence( this.evidencePath );
		assertEquals(adaptador.getText( bySuccessMessage ), "Success: Your password has been successfully updated." );
		
	}
	
	public void validationSuccessAddressAddedMessage() throws IOException {
		
		adaptador.takeEvidence( this.evidencePath );
		assertEquals(adaptador.getText( bySuccessMessage ), "Your address has been successfully added" );
		
	}
	
	public void validationSuccessAffiliateAccountMessage() throws IOException {
		
		adaptador.takeEvidence( this.evidencePath );
		assertEquals(adaptador.getText( bySuccessMessage ), "Success: Your account has been successfully updated." );
		
	}
	
	public void validationSubscribeNewsletterMessage() throws IOException {
		
		adaptador.takeEvidence( this.evidencePath );
		assertEquals(adaptador.getText( bySuccessMessage ), "Success: Your newsletter subscription has been successfully updated!" );
		
	}
	
	public void validationLogoutTittle() throws IOException {
		
		adaptador.takeEvidence( this.evidencePath );
		assertEquals(adaptador.getText( byLogoutTittle ), "Account Logout" );
		
	}
	
	public JSONObject readJSON( String jsonName ) throws IOException, ParseException {
		
		return adaptador.readJSON( jsonName );
		
	}
	
	public WebDriver getDriver() {
		
		return adaptador.getDriver();
		
	}
	
}
