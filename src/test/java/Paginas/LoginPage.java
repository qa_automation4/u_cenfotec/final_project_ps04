package Paginas;

import static org.testng.Assert.assertEquals;

import java.io.IOException;

import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import Adaptador.AdaptadorSelenium;



public class LoginPage {
	
	@FindBy(id="input-email")
	WebElement inputEmail;
	
	@FindBy(id="input-password")
	WebElement inputPassword;
	
	@FindBy(xpath="//input[@type='submit' and @value='Login']")
	WebElement btnLogin;
	
	By byWarningMessage  = By.xpath("//div[@class='alert alert-danger alert-dismissible']");
	By byMyAccountTittle = By.xpath("//div[@class='row']//h2[1]");
	
	String evidencePath = "";
	
	private AdaptadorSelenium adaptador;
	
	
	
	public LoginPage( String browserType, String driverPath, String evidencePath ) {
		
		adaptador         = AdaptadorSelenium.getAdaptador( browserType, driverPath );
		PageFactory.initElements( adaptador.getDriver(), this );
		this.evidencePath = evidencePath;
		
	}
	
	
	
	public void openUrl( String url ) {
		
		adaptador.openUrl( url );
		
	}
	
	public void closeDriver() {
		
		adaptador.closeDriver();
		
	}
	
	public void enterEmail( String email ) {
		
		adaptador.inputTextByElement( inputEmail, email );
		
	}
	
	public void enterPassword( String password ) {
		
		adaptador.inputTextByElement( inputPassword, password );

	}
	
	public void clickBtnLogin() {
		
		adaptador.toClickByElement( btnLogin );
		
	}
	
	public void validationWarningMessage() throws IOException {
		
		adaptador.takeEvidence( this.evidencePath );
		assertEquals(adaptador.getText( byWarningMessage ), "Warning: No match for E-Mail Address and/or Password." );
		
	}
	
	public void validationMyAccountTittle() throws IOException {
		
		adaptador.takeEvidence( this.evidencePath );
		assertEquals(adaptador.getText( byMyAccountTittle ), "My Account" );
		
	}
	
	public JSONObject readJSON( String jsonName ) throws IOException, ParseException {
		
		return adaptador.readJSON( jsonName );
		
	}
	
}
